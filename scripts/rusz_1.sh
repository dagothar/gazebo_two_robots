rostopic pub /ur5_1/arm_controller/follow_joint_trajectory/goal control_msgs/FollowJointTrajectoryActionGoal "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
goal_id:
  stamp:
    secs: 0
    nsecs: 0
  id: ''
goal:
  trajectory:
    header:
      seq: 0
      stamp:
        secs: 0
        nsecs: 0
      frame_id: ''
    joint_names:
    - 'ur5_1base'
    - 'ur5_1shoulder'
    - 'ur5_1elbow'
    - 'ur5_1wrist_1'
    - 'ur5_1wrist_2'
    - 'ur5_1wrist_3'
    points:
    - positions: [1, -1.57, 0, -1.57, 0, 0]
      time_from_start: {secs: 1, nsecs: 0}
  path_tolerance:
  - {name: '', position: 0.0, velocity: 0.0, acceleration: 0.0}
  goal_tolerance:
  - {name: '', position: 0.0, velocity: 0.0, acceleration: 0.0}
  goal_time_tolerance: {secs: 0, nsecs: 0}"
